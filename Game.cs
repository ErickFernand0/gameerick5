﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace ConsoleApp5Game
{
    class game
    {
        public void Start()
        {
            Title = "World of Zuma - The Game!";
            RunMainMenu();


        }

        private void RunMainMenu()
        {
            string prompt = @"
$$\      $$\                     $$\       $$\                  $$$$$$\        $$$$$$$$\                                  
$$ | $\  $$ |                    $$ |      $$ |                $$  __$$\       \____$$  |                                 
$$ |$$$\ $$ | $$$$$$\   $$$$$$\  $$ | $$$$$$$ |       $$$$$$\  $$ /  \__|          $$  /$$\   $$\ $$$$$$\$$$$\   $$$$$$\  
$$ $$ $$\$$ |$$  __$$\ $$  __$$\ $$ |$$  __$$ |      $$  __$$\ $$$$\              $$  / $$ |  $$ |$$  _$$  _$$\  \____$$\ 
$$$$  _$$$$ |$$ /  $$ |$$ |  \__|$$ |$$ /  $$ |      $$ /  $$ |$$  _|            $$  /  $$ |  $$ |$$ / $$ / $$ | $$$$$$$ |
$$$  / \$$$ |$$ |  $$ |$$ |      $$ |$$ |  $$ |      $$ |  $$ |$$ |             $$  /   $$ |  $$ |$$ | $$ | $$ |$$  __$$ |
$$  /   \$$ |\$$$$$$  |$$ |      $$ |\$$$$$$$ |      \$$$$$$  |$$ |            $$$$$$$$\\$$$$$$  |$$ | $$ | $$ |\$$$$$$$ |
\__/     \__| \______/ \__|      \__| \_______|       \______/ \__|            \________|\______/ \__| \__| \__| \_______|
                                                                                                                          
                                                                                                                          
Welcome to the World. What would you like to do? 
(Use the arrow keys to cycle through options and press enter to select an option.";

            string[] options = { "Play", "About", "Exit" };
            Menu mainMenu = new Menu(prompt, options);
            int selectedIndex = mainMenu.Run();

            switch (selectedIndex)
            {
                case 0:
                    RunFisrtChoice();
                    break;
                case 1:
                    DisplayAboutInfo();
                    break;
                case 2:
                    ExitGame();
                    break;
            }
        }

        private void ExitGame()
        {
            WriteLine("\nPress any key to exit...");
            ReadKey(true);
            Environment.Exit(0);
        }


        private void DisplayAboutInfo()
        {
            Clear();
            WriteLine("this game demo was created by Erick.");
            WriteLine("It uses assets from https://patorjk.com/software/taag.");
            WriteLine("This is just a demo... full game coming to console near you soon!");
            ReadKey(true);
            RunMainMenu();
        }

        private void RunFisrtChoice()
        {
            string prompt = "What Weapon do you wabt to use?";
            string[] options = { "Longbow(3000)", "Spear(2000)", "Dual Sword(5000)" };
            Menu colorMenu = new Menu(prompt, options);
            int selectedIndex = colorMenu.Run();

            BackgroundColor = ConsoleColor.White;
            switch (selectedIndex)
            {
                case 0:
                    ForegroundColor = ConsoleColor.Red;
                    WriteLine("\nHere is Your Weapon");
                    break;
                case 1:
                    ForegroundColor = ConsoleColor.Green;
                    WriteLine("\nHere is Your Weapon");
                    break;
                case 2:
                    ForegroundColor = ConsoleColor.Blue;
                    WriteLine("\nHere is Your Weapon");
                    break;
                case 3:
                    ForegroundColor = ConsoleColor.Yellow;
                    WriteLine("\nHere is Your Weapon");
                    break;
            }
            ResetColor();

            WriteLine("That.s all for this game demo!");
            ExitGame();


        }
    }
}
